<?php
/**
 * @file
 * views_example_features.features.inc
 */

/**
 * Implements hook_views_api().
 */
function views_example_features_views_api() {
  return array("version" => "3.0");
}
