-------------
Views Example
-------------

------------
Introduction
------------
The objective of this project is to illustrate a bug with Views 7.x-3.5 (and more generally Views 7.x-3.x) where an embedded view with exposed widgets is emptied incorrectly because of validation errors in unrelated forms on the same page as the view.

http://drupal.org/sandbox/amarnus/1863012
http://drupal.org/node/1863020

------------
Installation
------------
1. On enabling this module, navigate directly to /views-example or /index.php?q=views-example.
2. Force form validation failure of the form that has "Enter an integer only" by keying in a non-numeric string, hit Submit and observe that the view on the page has been emptied. (Shows "No results" text)